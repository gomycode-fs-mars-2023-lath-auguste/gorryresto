import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Pages/Home';
import Croissant from './Pages/Croissant';
import IceCream from './Pages/IceCream';
import './Styles/index.css'
import FormPlat from './Composants/FormPlat';
import Plat from './Composants/Plat';


const App = () => {

  const [plat, setPlat] = useState([]);

  const updateForm = (item) => {
    setPlat([...plat, item])
  }

  return (
    // <BrowserRouter>
    //   <Routes>
    //     <Route path='/' element={<Home/>} />
    //     <Route path='/croissant' element={<Croissant/>} />
    //     <Route path='/icecream' element={<IceCream/>} />
    //   </Routes>
    // </BrowserRouter>

    <div>
      <div className="container-fluid">
        <div className="row">
          <div className='col-9 border menuGauche'>
            <h1 className='display-3 text-center py-3'>Mondej</h1>
            <div className='row'>
              <Plat plat={plat} />
            </div>
          </div>
          <div className='col-3 menuDroit border'>
            <h2 className='fw-light text-center'>Current Order</h2>
          </div>
        </div>

      </div>
        <div className=" menuBas col-12 px-4">
          <h2 className='text-center' >Ajouter Un Nouveau Plat </h2>
          <FormPlat updateForm={updateForm} />
        </div>
    </div>
    
  );
}

export default App;
