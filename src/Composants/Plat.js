import React from 'react';

const Plat = ({plat}) => {
    return (
        <>
            {plat && plat.map((item, index) => { return (
                <div key={index} className="card p-0 col-2 m-2" >
                    {item.image && (
                        <img src={item.image} className="card-img-top h-50" alt="..."/>
                    )}
                    {/* <>{item.image} </> */}
                    <div className="card-body h-50">
                        <h5 className="card-title my-0">{item.name}</h5>
                        <p className="card-text my-0">{item.description}</p>
                    </div>
                    <div className='d-flex justify-content-between align-items-center zonePrix card-footer'>
                        <p className='my-0'>{item.price}$</p>
                        <a href="#" className="btn btn-primary">Ajouter</a>
                    </div>
                </div>
            )})}
        </>
    );
}

export default Plat;
