import React, { useState } from 'react';

const FormPlat = ({updateForm}) => {
    
    const [validated, setValidate] = useState(false)
    const [plat, setPlat] = useState({
        name:'',
        description:'',
        price: '',
        image: ''
    });

    const handleChange =(e) =>{
        setPlat({...plat, [e.target.name]: e.target.value})

    }

    const handleImageChange = (e) => {
        const file = e.target.files[0];
        setPlat({ ...plat, image: URL.createObjectURL(file) });
    };

    const handleSubmit = (e) =>{
        e.preventDefault();
        const formulaire = e.currentTarget;
        if ((formulaire.checkValidity() === false) || (plat.price  <= 1) || (plat.image == '')){
            e.stopPropagation()
            alert("Verifiez vos informations");
        }else{
            updateForm(plat);
            setValidate(true)
            setPlat({
                name:'',
                description:'',
                price: '',
                image: ''
            });
            alert("Plat ajouté");
        }
    }

    return (
        <>
            <form className="row needs-validation" noValidate validated={validated} onSubmit={handleSubmit}>

                    <div className="col-3">
                        <label htmlFor="name" className="form-label">Nom du Plat</label>
                        <input type="text" className="form-control" name='name' id="name" value={plat.name} required onChange={handleChange} />
                        <div className="valid-feedback">
                        Looks good!
                        </div>
                    </div>

                    <div className="col-3">                        
                        <label htmlFor="description" className="form-label">Description</label>
                        <input type="text" className="form-control" name='description' id="description" value={plat.description} required onChange={handleChange} />
                        <div className="valid-feedback">
                        Looks good!
                        </div>
                    </div>
                    <div className="col-2">                        
                        <label htmlFor="price" className="form-label">Prix</label>
                        <input type="number" className="form-control" min="1" name='price' id="price" value={plat.price} required onChange={handleChange} />
                        <div className="valid-feedback">
                        Looks good!
                        </div>
                    </div>

                <div className=" col mb-3">
                    <label htmlFor="photoPlat" className="form-label">Image</label>
                    <input type="file" className="form-control w-100 px-2" aria-label="file example"  name='image' id='photoPlat' required onChange={handleImageChange} />
                    <div className="invalid-feedback">Example invalid form file feedback</div>
                </div>

                <div className=" text-center">
                    <button className="btn btn-primary" type="submit">Submit form</button>
                </div>
            </form>
        </>
    );
}

export default FormPlat;
